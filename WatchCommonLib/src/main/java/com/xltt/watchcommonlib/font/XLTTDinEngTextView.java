package com.xltt.watchcommonlib.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class XLTTDinEngTextView extends androidx.appcompat.widget.AppCompatTextView {

    public XLTTDinEngTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = XLTTFontManager.FONT_TYPE_DING;
        if (typeface != null) {
            this.setTypeface(typeface);
            setIncludeFontPadding(false);
        }
    }
}
