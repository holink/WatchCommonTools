package com.xltt.watchcommonlib.font;

import android.content.Context;
import android.graphics.Typeface;

public class XLTTFontManager {
    public static Typeface FONT_TYPE_DING;
    public static Typeface FONT_TYPE_SIYUAN;

    /**
     * 应用启动请初始化字体
     *
     * @param context
     */
    public static void initFont(Context context) {
        FONT_TYPE_DING = Typeface.createFromAsset(context.getAssets(), "font/DINEng_Alternate.ttf");
        FONT_TYPE_SIYUAN = Typeface.createFromAsset(context.getAssets(), "font/Source-Normal.ttf");
    }
}
