package com.xltt.watchcommonlib.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

public class XLTTSiYuanNormalTextView extends androidx.appcompat.widget.AppCompatTextView {
    public XLTTSiYuanNormalTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = XLTTFontManager.FONT_TYPE_SIYUAN;
        if (typeface != null) {
            this.setTypeface(typeface);
            setIncludeFontPadding(false);
        }
    }
}
