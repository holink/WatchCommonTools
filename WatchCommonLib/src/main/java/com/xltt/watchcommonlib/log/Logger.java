package com.xltt.watchcommonlib.log;

import android.util.Log;

/**
 * 日志类
 */
public class Logger {
    private static String mAppName;
    private static boolean mIsDebug = true;

    public static void initLog(String appName, boolean isDebug) {
        mAppName = appName;
        mIsDebug = isDebug;
    }

    public static void i(String tag, String msg) {
        if (mIsDebug) {
            Log.i(tag, mAppName + " 日志-->" + msg);
        }
    }

    public static void e(String tag, String msg) {
        if (mIsDebug) {
            Log.e(tag, mAppName + " 日志-->" + msg);
        }
    }
}
