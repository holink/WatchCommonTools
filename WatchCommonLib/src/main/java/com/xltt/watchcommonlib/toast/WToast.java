package com.xltt.watchcommonlib.toast;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class WToast {
    private static Toast toast = null;

    public static void showToast(Context context, String msg) {
        if (toast != null) {
            toast.setText(msg);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.show();
        } else {
            toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
}
