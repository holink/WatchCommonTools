package com.xltt.watchcommonlib.view;

public interface XLTTSlideFinishListener {
    void onSlidingFinish();
}
