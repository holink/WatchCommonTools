package com.xltt.watchcommonlib.base;

import android.content.Intent;

public class BaseEvent {
    //事件标识
    private int eventId;
    //参数传递都可以放这个里面
    private Intent intent = new Intent();

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
}
