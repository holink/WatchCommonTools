package com.xltt.watchcommonlib.base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import butterknife.ButterKnife;

public abstract class BaseBindActivity extends BaseActivity {
    public final String TAG = "BaseBindActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initView();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        initView();
    }

    private void initView() {
        ButterKnife.bind(this);
        initSlide();
        setUpViews();
        setUpData();
    }

    public abstract void setUpViews();

    public abstract void setUpData();
}
