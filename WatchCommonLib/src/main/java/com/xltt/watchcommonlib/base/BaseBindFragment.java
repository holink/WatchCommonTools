package com.xltt.watchcommonlib.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;

public abstract class BaseBindFragment extends Fragment {
    public final String TAG = getClass().toString();
    public View viewGroup;
    public BaseActivity parent;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, int layoutId) {
        return viewGroup = inflater.inflate(layoutId, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parent = (BaseActivity) getActivity();
        ButterKnife.bind(this, viewGroup);
        this.setUpViews();
        this.setUpData();
    }

    /**
     * 调用顺序为先用调用 setUpViews   再使用 setUpData
     */
    public abstract void setUpViews();

    public abstract void setUpData();


    /**
     * 刷新界面的时候用，子类重载的方式
     */
    public void refreshPage(Object... object) {

    }
}
