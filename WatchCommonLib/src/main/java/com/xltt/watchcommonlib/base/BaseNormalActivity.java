package com.xltt.watchcommonlib.base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

public abstract class BaseNormalActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initSlide();
        setUpViews();
        setUpData();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        initSlide();
        setUpViews();
        setUpData();
    }

    public abstract void setUpViews();

    public abstract void setUpData();
}
