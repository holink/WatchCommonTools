package com.xltt.watchcommonlib.base;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.FragmentActivity;

import butterknife.ButterKnife;
import me.imid.swipebacklayout.lib.SwipeBackLayout;
import me.imid.swipebacklayout.lib.app.SwipeBackActivity;

/**
 * 此类不可在外面单独使用
 */
public class BaseActivity extends SwipeBackActivity {
    SwipeBackLayout swp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        if (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_CANCEL) {
//            sendTouchBroadCast(1);
//        } else if (ev.getAction() == MotionEvent.ACTION_DOWN) {
//            sendTouchBroadCast(2);
//        } else if (ev.getAction() == MotionEvent.ACTION_MOVE) {
//            sendTouchBroadCast(3);
//        }
//        return super.dispatchTouchEvent(ev);
//    }

    public void setContentView(View view, boolean withBind) {
        super.setContentView(view);
        if (withBind) {
            ButterKnife.bind(this);
        }
        initSlide();
    }

    public void setContentView(int layoutResID, boolean withBind) {
        super.setContentView(layoutResID);
        if (withBind) {
            ButterKnife.bind(this);
        }
        initSlide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 是否 禁止左滑退出
     *
     * @param enable
     */
    public void setSlideEnable(boolean enable) {
        if (swp != null) {
            swp.setEnableGesture(enable);
        }
    }

    /**
     * 刷新界面的时候用，子类重载的方式
     */
    public void refreshPage(Object... object) {

    }

    public void initSlide() {
        swp = getSwipeBackLayout();
        swp.setEdgeTrackingEnabled(SwipeBackLayout.EDGE_LEFT);
    }

//    @Override
//    public void startActivity(Intent intent) {
////        super.startActivity(intent);
//        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
//    }
//
//    @Override
//    public void startActivityForResult(Intent intent, int requestCode) {
////        super.startActivityForResult(intent, requestCode);
//        startActivityForResult(intent, requestCode, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
//    }

    public void startActivityNormal(Intent intent) {
        super.startActivity(intent);
    }

    public void startActivityForResultNormal(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
    }

    public Bundle makeAnimationBundle(Class activity) {
        Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                getRootView(),
                activity.toString())
                .toBundle();
        return bundle;
    }

    public View getRootView() {
        ViewGroup viewGroup = ((ViewGroup) findViewById(android.R.id.content));
        //设置颜色
        if (viewGroup != null && viewGroup.getChildCount() > 0) {
            View view = viewGroup.getChildAt(0);
            return view;
        }
        return null;
    }


    private void sendTouchBroadCast(int action) {
        Intent intent = new Intent("xltt_touch_screen_status");
        intent.putExtra("action", action);
        sendBroadcast(intent);
    }
}
