package com.xltt.commontools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.xltt.watchcommonlib.base.BaseActivity;
import com.xltt.watchcommonlib.base.BaseBindActivity;
import com.xltt.watchcommonlib.log.Logger;
import com.xltt.watchcommonlib.toast.WToast;

public class MainActivity extends BaseBindActivity {
    private static final String TAG = "MainActivity";

    private SlidingDrawerLayout mSlidingDrawer;
    private View mTopBtn, mBottomBtn;
    CBroadCast cBroadCast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void setUpViews() {
        setSlideEnable(true);

        WToast.showToast(this, "Unable to ");

//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction("xltt_touch_screen_status");
//        registerReceiver(cBroadCast = new CBroadCast(), intentFilter);
    }

    @Override
    public void setUpData() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterReceiver(cBroadCast);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
//        if (ev.getAction() == MotionEvent.ACTION_UP) {
//            startActivity(new Intent(this, TestActivity.class));
//        }
        return super.dispatchTouchEvent(ev);
    }


//    private void findView() {
//        mSlidingDrawer = (SlidingDrawerLayout) findViewById(R.id.slidingDrawer);
//        mTopBtn = findViewById(R.id.topBtn);
//        mBottomBtn = findViewById(R.id.bottomBtn);
//    }
//
//    private void initView() {
//        Resources res = getResources();
//        int topBarSize = (int) res.getDimension(R.dimen.topBarSize);
//        int bottomBarSize = (int) res.getDimension(R.dimen.bottomBarSize);
//        mSlidingDrawer.setTopTabHeight(topBarSize, true);
//        mSlidingDrawer.setBottomTabHeight(bottomBarSize, true);
//
//        mTopBtn.setOnClickListener(this);
//        mBottomBtn.setOnClickListener(this);
//
//        mSlidingDrawer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mSlidingDrawer.openTopSync();
//            }
//        });
//    }
//
//    @Override
//    public void onClick(View v) {
//        if (v.getId() == R.id.topBtn) {
//            if (mSlidingDrawer.isBottomOpened()) {
//                mSlidingDrawer.closeBottom();
//            } else {
//                if (mSlidingDrawer.isTopOpened()) {
//                    mSlidingDrawer.closeTop();
//                } else {
//                    mSlidingDrawer.openTopSync();
//                }
//            }
//        } else if (v.getId() == R.id.bottomBtn) {
//            if (mSlidingDrawer.isTopOpened()) {
//                mSlidingDrawer.closeTop();
//            } else {
//                if (mSlidingDrawer.isBottomOpened()) {
//                    mSlidingDrawer.closeBottom();
//                } else {
//                    mSlidingDrawer.openBottomSync();
//                }
//            }
//        }
//    }
//
//    public void bottomViewClick(View view) {
//        WToast.showToast(this, "xxxx");
//    }


    class CBroadCast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Logger.i(TAG, "action:" + +intent.getIntExtra("action", -1));
        }
    }
}