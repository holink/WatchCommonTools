# WatchCommonTools

#### 介绍
公共工具

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

圆形控件引用说明：

    <!--引用方式如下-->
    <com.xltt.watchcommonlib.view.WCircleProgressView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:padding="30dp"
        app:backWidth="20dp"
        app:progFirstColor="@color/colorAccent"
        app:progStartColor="@color/colorPrimary"
        app:progWidth="30dp"
        app:progress="65" />

    <!--属性如下-->
    <declare-styleable name="WCircleProgressView">
            <attr name="backWidth" format="dimension" />    <!--背景圆环宽度-->
            <attr name="progWidth" format="dimension" />    <!--进度圆环宽度-->
            <attr name="backColor" format="color" />        <!--背景圆环颜色-->
            <attr name="progColor" format="color" />        <!--进度圆环颜色-->
            <attr name="progStartColor" format="color" />   <!--进度圆环开始颜色-->
            <attr name="progFirstColor" format="color" />   <!--进度圆环结束颜色-->
            <attr name="progress" format="integer" />       <!--圆环进度-->
    </declare-styleable>

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
